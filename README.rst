===============================
drm/i915 style and static check
===============================

This is a (draft) project to:

* Document the existing, but unwritten, rules of `drm/i915 style`_.

* Implement simple `static checkers`_ for some of the style and other elements
  of drm/i915 driver code.

It's likely all of this will be merged to other places when closer to
completion, perhaps the `maintainer tools`_.


.. _drm/i915 style: i915-style-guide.rst

.. _static checkers: i915-check

.. _maintainer tools: https://gitlab.freedesktop.org/drm/maintainer-tools/
